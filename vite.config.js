import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';
import { resolve } from 'path';
import { mdsvex } from 'mdsvex';
import sharp from 'sharp';
import * as fs from 'fs';
import routify from '@roxi/routify/vite-plugin';
// import { dirname } from 'path';
// import { fileURLToPath } from 'url';
import genSitemap from 'routify-plugin-sitemap';
import sveltePreprocess from 'svelte-preprocess';
import autoprefixer from 'autoprefixer';

// const __dirname = dirname(fileURLToPath(import.meta.url));
let images = fs.readdirSync('./src/img/', { withFileTypes: true }).map(file => './src/img/' + file.name);
let imgLen = images.length;
for (let i = 0; i < imgLen; i++) {
  sharp(fs.readFileSync(images[i])).webp().toFile('./public/assets/' + images[i].replace(/\.[^\.]*?$/, '').split('/').at(-1) + ".webp", err => err != undefined ? console.log(err) : undefined);
}

// https://vitejs.dev/config/
export default defineConfig({
  // resolve: {
  //   alias: {
  //     '~': resolve(__dirname, './'),
  //     '@src': resolve(__dirname, 'src')
  //   }
  // },
  plugins: [
    routify({
      extensions: ['.svelte', '.svx', '.md'],
      plugins: [genSitemap({ baseUrl: 'https://tgocala.com', dir: 'public' }),
        "routify-plugin-frontmatter"
      ]
    }),
    svelte({
      extensions: ['.svelte', '.svx', '.md'],
      emitCss: true,
      preprocess: [
        mdsvex({ extensions: ['.md'], layout: {blog:'src/routes/blog/_layout.svelte'} }),
        sveltePreprocess()
      ]
    })
  ],
  css: {
    postcss: {
      plugins: [
        autoprefixer()
      ]
    }
  },
  rollupDedupe: ['svelte'],
  build: {
    lib: {
      entry: 'src/main.js',
      formats: ['iife'],
      name: 'svelte',
      fileName: 'svelte'
    }
  }
})
